from .models import Prime

def primes():
    yield 2; yield 3; yield 5; yield 7
    bps = (p for p in primes())
    p = next(bps) and next(bps)
    q = p * p
    sieve = {}
    n = 9
    while True:
        if n not in sieve:
            if n < q:
                yield n
            else:
                p2 = p + p
                sieve[q + p2] = p2
                p = next(bps)
                q = p * p
        else:
            s = sieve.pop(n)
            nxt = n + s
            while nxt in sieve:
                nxt += s
            sieve[nxt] = s
        n += 2

def get_nthprime(n):
    try:
        lp = Prime.objects.all().reverse()[0]
        lpn = int(lp.number)
    except:
        lpn = 0
    a = primes()
    for x in range(1, n):
        if lpn >= x:
            next(a)
            continue;
        q = Prime(nthprime = next(a), number = x)
        q.save()
    return next(a)
