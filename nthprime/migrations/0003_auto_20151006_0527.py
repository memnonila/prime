# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nthprime', '0002_auto_20151001_2140'),
    ]

    operations = [
        migrations.AlterField(
            model_name='prime',
            name='nthprime',
            field=models.IntegerField(default=0, unique=True),
        ),
        migrations.AlterField(
            model_name='prime',
            name='number',
            field=models.IntegerField(default=0, unique=True),
        ),
    ]
