from django.db import models

class Prime(models.Model):
    nthprime = models.IntegerField(default=0, unique=True)
    number = models.IntegerField(default=0, unique=True)

    def __str__(self):
        return str(self.number)

    class Meta:
        ordering = ['number']
