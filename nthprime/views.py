from django.shortcuts import render

from .forms import PrimeForm
from .models import Prime

from .compute import get_nthprime

def get_prime(request):

    if request.method == 'POST':
        form = PrimeForm(request.POST)

        if form.is_valid():
            n = form.cleaned_data['number']
            result = {}
            try:
                r = Prime.objects.get(number=n)
                result['prime'] = r.nthprime
                result['number'] = n
            except:
                result['number'] = n
                result['prime'] = get_nthprime(int(n))
                q = Prime(nthprime = result['prime'], number = int(n))
                q.save()
            return render(request, 'results.html', {'result': result})
    else:
        form = PrimeForm()

    return render(request, 'index.html', {'form': form})
